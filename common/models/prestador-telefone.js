'use strict';

module.exports = function(Prestadortelefone) {
    Prestadortelefone.on('dataSourceAttached', function(obj) {
        Prestadortelefone.custom.autocomplete.tipo_telefone = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.tipo_telefone.nome;
            }
        };
    })

};
