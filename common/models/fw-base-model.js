'use strict';

// var _ = require('underscore');
// var g = require('strong-globalize')();
// var loopback = require('loopback');

module.exports = function(FwBaseModel) {
    // debugger;
    // var app = require('../../server/server');

    // FwBaseModel.custom = {};
    // FwBaseModel.custom.autocomplete = { 'a': 'b' };

    // //backup for parent method;
    // FwBaseModel._setupRemoting = FwBaseModel.setupRemoting;

    // FwBaseModel.setupRemoting = function() {
    //     this._setupRemoting();

    //     var FwBaseModel = this;
    //     var typeName = FwBaseModel.modelName;
    //     var options = FwBaseModel.settings;



    //     function setRemoting(scope, name, options) {
    //         var fn = scope[name];
    //         // fn._delegate = true;
    //         options.isStatic = scope === FwBaseModel;
    //         FwBaseModel.remoteMethod(name, options);
    //     }

    //     setRemoting(FwBaseModel, 'pager', {
    //         description: 'Retrieves rows with pager settings',
    //         accepts: [{
    //             arg: 'req',
    //             type: 'object',
    //             model: typeName,
    //             http: {
    //                 source: 'req'
    //             }
    //         }],
    //         returns: {
    //             arg: 'rows',
    //             type: typeName,
    //             root: true,
    //             description:
    //                 ('Return rows retrieved'),
    //         },
    //         http: {
    //             verb: 'get',
    //             path: '/pager'
    //         }
    //     });

    //     setRemoting(FwBaseModel, 'pagerDetail', {
    //         description: 'Retrieves rows with pager settings',
    //         accepts: [{
    //             arg: 'req',
    //             type: 'object',
    //             model: typeName,
    //             http: {
    //                 source: 'req'
    //             }
    //         }],
    //         returns: {
    //             arg: 'rows',
    //             type: typeName,
    //             root: true,
    //             description:
    //                 ('Return rows retrieved'),
    //         },
    //         http: {
    //             verb: 'get',
    //             path: '/details/:id/:detail/pager'
    //         }
    //     });

    //     setRemoting(FwBaseModel, 'postDetail', {
    //         isStatic: false,
    //         http: { verb: 'post', path: '/details/:detail/:id' },
    //         accepts: this._removeOptionsArgIfDisabled([{
    //                 arg: 'data',
    //                 type: 'object',
    //                 model: typeName,
    //                 http: { source: 'req' },
    //             },
    //             { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    //         ]),
    //         //   description: format('Creates a new instance in %s of this model.', relationName),
    //         accessType: 'WRITE',
    //         returns: { arg: 'data', type: typeName, root: true }
    //     });

    //     setRemoting(FwBaseModel, 'deleteDetail', {
    //         isStatic: false,
    //         http: { verb: 'delete', path: '/details/:id/:detail/:iddetail' },
    //         accepts: this._removeOptionsArgIfDisabled([{
    //                 arg: 'data',
    //                 type: 'object',
    //                 model: typeName,
    //                 http: { source: 'req' },
    //             },
    //             { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    //         ]),
    //         //   description: format('Creates a new instance in %s of this model.', relationName),
    //         accessType: 'WRITE',
    //         returns: { arg: 'data', type: typeName, root: true }
    //     });



    //     setRemoting(FwBaseModel, 'autocomplete', {
    //         description: 'retrieves options for autocomplete field',
    //         accepts: [{
    //                 arg: 'params',
    //                 type: 'object',
    //                 required: true,
    //                 http: function(ctx) {

    //                     var req = ctx.req.params;

    //                     return req;
    //                 }
    //             }
    //             // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
    //         ],
    //         returns: {
    //             arg: 'rows',
    //             type: 'object',
    //             root: true,
    //             description:
    //                 ('Return rows retrieved'),
    //         },
    //         http: {
    //             verb: 'get',
    //             path: '/autocomplete/:field/:search'
    //         }
    //     });


    //     setRemoting(FwBaseModel, 'autocompleteDetail', {
    //         description: 'retrieves options for autocomplete field',
    //         accepts: [{
    //                 arg: 'params',
    //                 type: 'object',
    //                 required: true,
    //                 http: function(ctx) {

    //                     var req = ctx.req.params;

    //                     return req;
    //                 }
    //             }
    //             // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
    //         ],
    //         returns: {
    //             arg: 'rows',
    //             type: 'object',
    //             root: true,
    //             description:
    //                 ('Return rows retrieved'),
    //         },
    //         http: {
    //             verb: 'get',
    //             path: '/details/:detail/autocomplete/:field/:search'
    //                 // Route::get($route["route"] . "/details/{detail}/autocomplete/{field}/{search?}", $route["controller"] . '@autocompleteDetail');
    //         }
    //     });

    //     setRemoting(FwBaseModel, 'crudGET', {
    //         description: 'Find a model instance by {{id}} from the data source.',
    //         accessType: 'READ',
    //         accepts: this._removeOptionsArgIfDisabled([{
    //                 arg: 'id',
    //                 type: 'any',
    //                 description: 'Model id',
    //                 required: true,
    //                 http: { source: 'path' }
    //             },
    //             { arg: 'options', type: 'object', http: 'optionsFromRequest' },
    //         ]),
    //         returns: { arg: 'data', type: typeName, root: true },
    //         http: { verb: 'get', path: '/crudGET/:id' }
    //     });

    //     FwBaseModel.crudGET = function(id, fn) {
    //         // console.log(arguments);
    //         // var id = req;

    //         var modelName = FwBaseModel.definition.name;
    //         var relations = FwBaseModel.definition.settings.relations;

    //         var includes = [];
    //         var labels = [];
    //         // if (relations.length > 0) {
    //         _.each(relations, function(rel, key) {
    //             // console.log(rel.type);
    //             if (rel.type == 'belongsTo' || rel.type == 'hasMany') {

    //                 if (rel.type == 'belongsTo') {
    //                     labels.push(key);
    //                 }

    //                 var nestedRelations = app.models[rel.model].settings.relations;

    //                 var nestedIncludes = [];
    //                 _.each(nestedRelations, function(nRel, nKey) {

    //                     if (nRel.type == 'belongsTo') {
    //                         nestedIncludes.push(nKey);
    //                     }
    //                 });

    //                 if (nestedIncludes.length == 0) {
    //                     includes.push(key);
    //                 } else {
    //                     var _o = {};
    //                     _o[key] = nestedIncludes;
    //                     includes.push(_o);
    //                 }


    //             }
    //         });

    //         // console.log(includes);

    //         FwBaseModel.findById(id, {
    //             include: includes
    //         }, function(err, row) {
    //             if (err) {
    //                 fn(err)
    //             }

    //             var newRow = (JSON.parse(JSON.stringify(row)));

    //             // console.log(row);

    //             // console.log(includes);

    //             console.log(newRow);

    //             includes.forEach(function(include) {
    //                 if (typeof include == 'string') {
    //                     var __relation = relations[include];
    //                     if (__relation != undefined) {
    //                         if (__relation.type == 'belongsTo') {
    //                             if (newRow[include] != undefined) {
    //                                 newRow[include + '.label'] = FwBaseModel.custom.autocomplete[include].label(newRow);
    //                             }
    //                         } else if (__relation.type == 'hasMany') {
    //                             console.log('esse caso ainda não foi implementado.', __relation);
    //                         }
    //                     }
    //                 } else if (typeof include == 'object') {
    //                     // include.forEach(function(val, key) {
    //                     _.each(include, function(val, key) {
    //                         var __relation = relations[key];
    //                         if (__relation != undefined) {
    //                             if (__relation.type == 'belongsTo') {
    //                                 if (newRow[key] != undefined) {
    //                                     newRow[key + '.label'] = FwBaseModel.custom.autocomplete[key].label(newRow);
    //                                 }
    //                             } else if (__relation.type == 'hasMany') {
    //                                 var subModel = app.models[__relation.model];
    //                                 if (typeof val == 'array' || typeof val == 'object') {
    //                                     //esse caso pode ser recursivo ou pode ser um objeto
    //                                     // newRow[key].foreach(function(_newRow) {

    //                                     if (newRow != null) {
    //                                         _.each(newRow[key], function(_newRow, _newRowKey) {

    //                                             _.each(val, function(_include) {
    //                                                 // console.log('include', _include, 'newRow', _newRow);
    //                                                 // console.log('_newRow[_include]', _newRow[_include]);
    //                                                 if (_newRow[_include] != undefined) {
    //                                                     // console.log('return', subModel.custom.autocomplete[_include].label(_newRow));
    //                                                     _newRow[_include + '.label'] = subModel.custom.autocomplete[_include].label(_newRow);
    //                                                 }
    //                                             })

    //                                             // console.log(_newRow);
    //                                             newRow[key][_newRowKey] = _newRow;
    //                                         });
    //                                     }
    //                                     // } else if (typeof val == 'object') {
    //                                     //     console.log('esse caso ainda não foi implementado 1:', val);
    //                                 } else {
    //                                     console.log('esse caso ainda não foi implementado 2: ', val)
    //                                 }
    //                             }
    //                         }
    //                     })
    //                 }

    //             });

    //             // labels.forEach(function(relation) {
    //             //     if (newRow[relation] != undefined) {
    //             //         newRow[relation + '.label'] = FwBaseModel.custom.autocomplete[relation].label(newRow);
    //             //     }
    //             // });


    //             // console.log(newRow);

    //             fn(null, newRow);
    //         });
    //     }



    //     function getRelations(model) {
    //         return model.definition.settings.relations;
    //     }

    //     function getRelation(model, relationName) {
    //         var relations = model.definition.settings.relations;

    //         if (relations[relationName]) {
    //             var _relation = relations[relationName];

    //             return _relation;
    //         }
    //     }

    //     FwBaseModel.deleteDetail = function(req, fn) {
    //         var detail = req.params.detail;
    //         var id = req.params.id;
    //         var iddetail = req.params.iddetail;

    //         // var data = req.body;

    //         var relation = getRelation(FwBaseModel, detail);
    //         var model = app.models[relation.model];

    //         // data[relation.foreignKey] = id;

    //         console.log(id, iddetail);
    //         var where = {
    //             id: iddetail,
    //         }
    //         where[relation.foreignKey] = id;

    //         model.destroyAll(where, function(err, success) {
    //             if (err) {
    //                 fn(err);
    //             } else {
    //                 fn(null, 'success');
    //             }
    //         });


    //     }

    //     FwBaseModel.postDetail = function(req, fn) {
    //         var detail = req.params.detail;
    //         var id = req.params.id;

    //         var data = req.body;

    //         var relation = getRelation(FwBaseModel, detail);
    //         var model = app.models[relation.model];

    //         data[relation.foreignKey] = id;
    //         model.create(data, function(err, success) {
    //             if (err) {
    //                 fn(err);
    //             } else {
    //                 fn(null, 'success');
    //             }
    //         });

    //         // fn('err');
    //     }

    //     FwBaseModel.pager = function(req, fn) {
    //         this._pager(FwBaseModel, req, fn);
    //     }

    //     FwBaseModel.pagerDetail = function(req, fn) {
    //         // console.log(req);
    //         //params: { id: '1', detail: 'telefones' }
    //         var params = req.params;
    //         var id = params.id;

    //         var relation = getRelation(FwBaseModel, params.detail);

    //         var model = app.models[relation.model];

    //         // console.log(model);

    //         model._crudWhere = {};

    //         model._crudWhere[relation.foreignKey] = id;

    //         this._pager(model, req, fn);
    //     }

    //     FwBaseModel._pager = function(model, req, fn) {

    //         var query = req.query;
    //         query.page = query.page || 1;

    //         if (model._crudWhere == undefined) {
    //             model._crudWhere = {};
    //         }

    //         var where = Object.assign({}, model._crudWhere);

    //         console.log('query.q:', query.q);

    //         debugger;
    //         if (query.q != undefined && query.q.trim().length > 0) {
    //             // model.definition

    //             var searchWhere = [];

    //             _.each(model.definition.properties, function(prop, key) {
    //                 // console.log(key, prop.type.name);

    //                 if (prop.type.name == 'String') {
    //                     console.log('entrou', key)
    //                     var tempObj = new Object();
    //                     tempObj[key] = new RegExp(query.q.trim(), 'i')
    //                     searchWhere.push(tempObj);
    //                 } else if (prop.type.name == 'Number') {

    //                 }
    //             });

    //             console.log(searchWhere);

    //             if (searchWhere.length > 0) {
    //                 console.log(where);
    //                 if (_.size(where) == 0) {
    //                     console.log('abc');
    //                     where['or'] = searchWhere;
    //                 } else {
    //                     console.log('cba');
    //                     where = {
    //                         'and': [
    //                             where,
    //                             searchWhere
    //                         ]
    //                     }
    //                 }
    //                 // where['or'] = searchWhere;
    //                 // console.log('entrou');
    //             }
    //         }

    //         // console.log(where);


    //         var limit = 20;
    //         // var filter = {
    //         //     limit: limit
    //         //     // skip: (query.page - 1) * limit
    //         // };

    //         var count = model.count(model._crudWhere, function(err, count) {
    //             // console.log(arguments);
    //             if (err) {
    //                 console.log(err);
    //                 fn(err);
    //             } else {
    //                 var modelName = model.definition.name;

    //                 var relations = getRelations(model);

    //                 var includes = [];
    //                 // if (relations.length > 0) {
    //                 _.each(relations, function(rel, key) {
    //                     // console.log(rel.type);
    //                     if (rel.type == 'belongsTo' || (model.customIncludes != undefined && model.customIncludes.indexOf(key) != -1)) {
    //                         var nestedRelations = app.models[rel.model].settings.relations;

    //                         var nestedIncludes = [];
    //                         _.each(nestedRelations, function(nRel, nKey) {

    //                             if (nRel.type == 'belongsTo') {
    //                                 nestedIncludes.push(nKey);
    //                             }
    //                         });

    //                         if (nestedIncludes.length == 0) {
    //                             includes.push(key);
    //                         } else {
    //                             var _o = {};
    //                             _o[key] = nestedIncludes;
    //                             includes.push(_o);
    //                         }

    //                     }

    //                 });

    //                 console.log(includes);

    //                 var _sort = 'createdAt DESC';

    //                 if (query.sort_by != undefined && query.order != undefined) {
    //                     _sort = query.sort_by + ' ' + query.order;
    //                 }

    //                 var filter = {
    //                     include: includes,
    //                     limit: limit,
    //                     skip: (query.page - 1) * limit,
    //                     order: _sort,
    //                     where: where
    //                 };


    //                 // filter = _.extend(model._crudWhere, filter);

    //                 // console.log(filter);

    //                 var rows = model.find(filter, function(err, rows) {
    //                     if (err) {
    //                         fn(err);
    //                     } else {
    //                         var newRows = [];

    //                         rows.forEach(function(row) {
    //                             var newRow = (JSON.parse(JSON.stringify(row)));
    //                             console.log('includes:', includes);
    //                             includes.forEach(function(relation) {
    //                                 console.log('antes entrar', model.definition.name, relation);
    //                                 var relationName = typeof relation == 'string' ? relation : Object.keys(relation)[0];
    //                                 if (model.custom.autocomplete[relationName] != undefined && newRow[relationName] != undefined) {
    //                                     console.log('tango', relationName);
    //                                     newRow[relationName + '.label'] = model.custom.autocomplete[relationName].label(newRow);
    //                                     // console.log('down', model.custom.autocomplete[relation]);
    //                                     // console.log('entrou', relation + '.label');
    //                                     // console.log(newRow);
    //                                 }
    //                             });

    //                             // console.log(newRow);

    //                             newRows.push(newRow);
    //                         });

    //                         var out = {
    //                             data: newRows,
    //                             page: req.page,
    //                             per_page: 20,
    //                             total_entries: rows.length,
    //                             total_count: count,
    //                             total_pages: Math.ceil(count / limit)
    //                         }



    //                         // console.log(rows);

    //                         fn(null, out);
    //                     }
    //                 });
    //             }
    //         });

    //     };

    //     FwBaseModel.autocomplete = autocomplete;
    //     FwBaseModel.autocompleteDetail = autocompleteDetail;

    //     function _autocomplete(modelName, params, fn) {
    //         console.log('AUTOCOMPLETE!', modelName, params);


    //         var relations = app.models[modelName].definition.settings.relations;
    //         var _field = params.field;

    //         if (relations[_field]) {
    //             var _relation = relations[_field];

    //             // console.log(_relation);

    //             var relationModel = app.models[_relation.model];


    //             var _where = {};

    //             // console.log(_field);
    //             if (!relationModel.custom.autocomplete[_field]) {
    //                 fn('autocomplete settings not defined. check custom.autocomplete');
    //             }

    //             var autocompleteSettings = relationModel.custom.autocomplete[_field];

    //             if (params.search != '[blank]') {

    //                 var reg = new RegExp(params.search, 'i');

    //                 _where = {
    //                     where: autocompleteSettings.where(reg)
    //                 };
    //             }

    //             _where.limit = 10;

    //             relationModel.find(_where, function(err, result) {
    //                 if (err) {
    //                     fn(err);
    //                 }
    //                 // console.log(result);

    //                 var out = [];

    //                 result.forEach(function(row) {
    //                     out.push(autocompleteSettings.out(row));
    //                 });

    //                 fn(null, out);
    //             });
    //         }
    //     }



    //     function autocompleteDetail(params, fn) {
    //         // console.log(arguments);

    //         var relations = FwBaseModel.definition.settings.relations;
    //         // var _field = params.field;
    //         var detail = params.detail;

    //         if (relations[detail]) {
    //             var modelName = relations[detail].model;

    //             // console.log(relations[detail]);

    //             _autocomplete(modelName, params, fn);
    //         }
    //     }

    //     function autocomplete(params, fn) {
    //         var modelName = FwBaseModel.definition.name;

    //         _autocomplete(modelName, params, fn);
    //     }
    // }

    // FwBaseModel.observe('before save', function verifyForeignKeys(ctx, next) {
    //     var app = ctx.Model.app;
    //     var modelName = ctx.Model.definition.name;

    //     // console.log('down', ctx);
    //     // debugger;
    //     if (ctx.instance) { // for single model update
    //         // console.log(ctx);

    //         // console.log('left');
    //         // console.log(ctx.instance);
    //         // Get the Application object which the model attached to, and we do what ever we want

    //         var s = ctx.instance;
    //         ctx.options.originalData = s.__data;

    //         // console.log(ctx.options.originalData);


    //         var relations = ctx.Model.definition.settings.relations;

    //         if (_.toArray(relations).length > 0) {
    //             // console.log('right');
    //             for (var y in relations) {

    //                 var relation = relations[y];

    //                 // console.log(relation);
    //                 if (relation.type == 'hasMany') {
    //                     // next();
    //                 } else {



    //                     var fkField = s.__data[relation.foreignKey];

    //                     if (relation.options == undefined) {
    //                         relation.options = {
    //                             required: false
    //                         };
    //                     }

    //                     if (relation.options.required == undefined) {
    //                         relation.options.required = false;
    //                     }

    //                     // console.log('up');
    //                     if (fkField !== undefined && fkField !== null && fkField !== '') {
    //                         if (relation.options.required && (fkField == undefined || fkField == null)) {

    //                             var createErr = new Error(relation.foreignKey + " can't be blank");
    //                             createErr.statusCode = 422;
    //                             createErr.code = 'CANT_SAVE_MODEL';

    //                             next(createErr);
    //                         } else {

    //                             // console.log(fkField, ctx.instance);

    //                             //PersistedModel.exists(id, callback ((err, exists)))
    //                             app.models[relation.model].exists(fkField, function(err, exists) {
    //                                 // console.log(fkField);

    //                                 if (err) throw err;
    //                                 if (!exists) {
    //                                     var createErr = new Error("Reference for Foreign Key " + relation.foreignKey + " for model " + relation.model + " does not exist");
    //                                     createErr.statusCode = 422;
    //                                     createErr.code = 'CANT_SAVE_MODEL';

    //                                     next(createErr);
    //                                 }

    //                             });
    //                         }
    //                         // next();
    //                     }
    //                     // else {
    //                     //     next();
    //                     // }


    //                     // });

    //                     console.log(y);
    //                     if (ctx.instance[y + '.label'] != undefined) {
    //                         ctx.instance[relation.foreignKey] = ctx.instance[y + '.label'].id;
    //                     }

    //                 }
    //             };
    //             // found 0 validation errors. proceed

    //             // console.log(relation.foreignKey, y, ctx.instance);


    //             // console.log(ctx.instance[y + '.label'].id, ctx.instance[y]);
    //             // console.log('tango before save');

    //             next();

    //         } else {
    //             next();
    //         }
    //     } else if (ctx.currentInstance) {

    //         //finish here
    //         // ctx.currentInstance.cdoe_id = 1;

    //         // console.log(ctx.currentInstance, ctx.data, ctx.Model);

    //         var relations = ctx.Model.definition.settings.relations;
    //         var _data = ctx.data;

    //         ctx.options.originalData = _data;

    //         if (_.toArray(relations).length > 0) {
    //             // console.log('right');
    //             for (var y in relations) {

    //                 var relation = relations[y];

    //                 if (relation.type == 'hasMany') {

    //                     // console.log(relation);

    //                     // console.log(ctx.currentInstance[y]);

    //                     // for (var z in _data[y]) {
    //                     //     var detail = _data[y][_z];
    //                     //     ctx.currentInstance[y].create()
    //                     // }

    //                     // next();
    //                 } else {



    //                     var fkField = _data[relation.foreignKey];

    //                     if (relation.options == undefined) {
    //                         relation.options = {
    //                             required: false
    //                         };
    //                     }

    //                     if (relation.options.required == undefined) {
    //                         relation.options.required = false;
    //                     }

    //                     // console.log('up');
    //                     if (fkField !== undefined && fkField !== null && fkField !== '') {
    //                         if (relation.options.required && (fkField == undefined || fkField == null)) {

    //                             var createErr = new Error(relation.foreignKey + " can't be blank");
    //                             createErr.statusCode = 422;
    //                             createErr.code = 'CANT_SAVE_MODEL';

    //                             next(createErr);
    //                         } else {

    //                             console.log(fkField, ctx.instance);

    //                             //PersistedModel.exists(id, callback ((err, exists)))
    //                             app.models[relation.model].exists(fkField, function(err, exists) {
    //                                 // console.log(fkField);

    //                                 if (err) throw err;
    //                                 if (!exists) {
    //                                     var createErr = new Error("Reference for Foreign Key " + relation.foreignKey + " for model " + relation.model + " does not exist");
    //                                     createErr.statusCode = 422;
    //                                     createErr.code = 'CANT_SAVE_MODEL';

    //                                     next(createErr);
    //                                 }

    //                             });
    //                         }
    //                         // next();
    //                     }
    //                     // else {
    //                     //     next();
    //                     // }


    //                     // });

    //                     if (_data[y + '.label'] != undefined) {
    //                         ctx.data[relation.foreignKey] = _data[y + '.label'].id;
    //                     }

    //                 }

    //             };
    //             // found 0 validation errors. proceed

    //             // console.log(relation.foreignKey, y, ctx.instance);


    //             // console.log(ctx.instance[y + '.label'].id, ctx.instance[y]);
    //             // console.log('tango before save');

    //             next();

    //         } else {
    //             next();
    //         }

    //     } else {
    //         next();
    //     }
    // });

    // FwBaseModel.observe('after save', function afterSave(ctx, next) {
    //     // console.log(arguments);
    //     // debugger;
    //     var originalData = (ctx.options.originalData);

    //     var app = ctx.Model.app;
    //     var modelName = ctx.Model.definition.name;
    //     // console.log('down', modelName);
    //     if (ctx.instance) { // for single model update
    //         console.log('left');
    //         var relations = ctx.Model.definition.settings.relations;
    //         var _data = ctx.instance;

    //         originalData = _.extend(originalData, ctx.instance.__data, ctx.instance.__cachedRelations);
    //         // console.log('tango', _new);
    //         // console.log(originalData);

    //         app.models[modelName].findById(_data.id, function(err, row) {

    //             _.each(relations, function(relation, y) {

    //                 // console.log('down');
    //                 if (relation.type == 'hasMany') {

    //                     var _model = app.models[relation.model];

    //                     // var oldData = [];
    //                     if (row[y] != undefined && row[y] != null) {
    //                         row[y]({}, function(err, old) {
    //                             // oldData = old;
    //                             // console.log('old', old);
    //                             var keepIds = [];

    //                             originalData[y] = originalData[y] || [];

    //                             originalData[y].forEach(function(detail, z) {
    //                                 if (detail.id != null || detail.id != undefined) {
    //                                     keepIds.push(detail.id);
    //                                 }
    //                             });

    //                             console.log('keepIds', keepIds);

    //                             var filterId = {};
    //                             filterId[relation.foreignKey] = row.id;

    //                             _model.destroyAll({
    //                                     and: [{
    //                                             id: { nin: keepIds }
    //                                         },
    //                                         filterId
    //                                     ]
    //                                 },
    //                                 function(err, success) {
    //                                     if (err) {
    //                                         throw err;
    //                                     }

    //                                     // for (var z in originalData[y]) {
    //                                     originalData[y].forEach(function(detail, z) {

    //                                         // console.log(detail);
    //                                         detail[relation.foreignKey] = row.id;

    //                                         // app.models[relation.model]
    //                                         _model.upsert(detail, function(err, ret) {
    //                                             // console.log(arguments);
    //                                         })
    //                                     })
    //                                 });
    //                         });
    //                     }

    //                 }
    //             });
    //         });

    //         next();
    //         // } else if (ctx.currentInstance) {
    //         //     var relations = ctx.Model.definition.settings.relations;
    //         //     var _data = ctx.data;

    //         //     if (_.toArray(relations).length > 0) {
    //         //         console.log('right');
    //         //         console.log(relations);

    //         //     }

    //         // next();
    //     } else {
    //         next();
    //     }

    // })

    // return FwBaseModel;
};