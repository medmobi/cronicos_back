'use strict';

var LoopBackContext = require('loopback-context');

module.exports = function(Feriado) {
    Feriado.on('dataSourceAttached', function(obj) {
        Feriado.custom.autocomplete.tipoFeriado = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {


                return row.tipoFeriado.nome;
            }
        };
    })

    Feriado.observe('before save', function feriadoBeforeSave(ctx, next) {

        if (ctx.instance && ctx.isNewInstance) { //update
            var instanceNome = ctx.instance.nome.toLowerCase();
            var instanceData = ctx.instance.data;

            var _ctx = LoopBackContext.getCurrentContext();
            var currentUser = _ctx && _ctx.get('currentUser');

            Feriado.find({
                    where: {
                        emp_id: currentUser.emp_id
                    }
                })
                .then(function (feriados) {
                    feriados.forEach(function (_feriado) {
                        var _feriadoNome = _feriado.nome.toLowerCase();
                        if (instanceNome === _feriadoNome && new Date(instanceData).getTime() === new Date(_feriado.data).getTime()) {
                            var createErr = new Error("Esse feriado já existe! Insira outra data ou nome.");
                            createErr.statusCode = 422;
                            createErr.code = 'CANT_SAVE_MODEL';

                            next(createErr);
                            return;
                        }
                    });

                    next();
                });
        } else {
            next();
        }

    });
};
