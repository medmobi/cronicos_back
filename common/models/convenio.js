'use strict';

module.exports = function(Convenio) {
    Convenio.validatesUniquenessOf('codigo', {message: 'Convênio já existente!'});

    Convenio.customIncludes = ['programas'];

    Convenio.observe('before save', function convenioBeforeSave(ctx, next) {
        if (ctx.instance && ctx.isNewInstance) { //update

            if (ctx.instance.__data.situacao == undefined || ctx.instance.__data.situacao == false) {
                var createErr = new Error("Não é possível criar um convênio com a situação inativa.");
                createErr.statusCode = 422;
                createErr.code = 'CANT_SAVE_MODEL';

                next(createErr);
            } else {
                next();
            }

        } else {
            next();
        }

    });

    Convenio.on('dataSourceAttached', function(obj) {
        // Convenio = this;
        Convenio.custom.autocomplete.conveniado = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {

                return row.conveniado.nome;
            }
        };
    });

};
