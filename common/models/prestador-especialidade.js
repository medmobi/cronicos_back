'use strict';

module.exports = function(Prestadorespecialidade) {
    Prestadorespecialidade.on('dataSourceAttached', function(obj) {
        Prestadorespecialidade.custom.autocomplete.especialidade = {
            where: function(reg) {
                return {
                    descricao: (reg),
                    situacao: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.descricao
                };
                return newRow;
            },
            label: function(row) {
                return row.especialidade.descricao;
            }
        };
    })

};