'use strict';

module.exports = function(Conveniado) {
    Conveniado.customIncludes = ['enderecos'];
    Conveniado.validatesUniquenessOf('nome', {message: 'Conveniado já existente!'});
};
