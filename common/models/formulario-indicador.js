'use strict';

module.exports = function(Formularioindicador) {
    Formularioindicador.on('dataSourceAttached', function(obj) {
        Formularioindicador.custom.autocomplete.indicador = {
            where: function(reg) {
                return {
                    descricao: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.codigo + ' - ' + row.descricao
                };
                return newRow;
            },
            label: function(row) {
                return row.indicador.descricao;
            }
        };
    })
};