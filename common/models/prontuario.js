'use strict';

module.exports = function(Prontuario) {
    var app = require('../../server/server');

    Prontuario._pager = function(model, req, fn) {
        return app.models.Paciente._pager(model, req, fn);
    };
};
