'use strict';

module.exports = function(Pacienteplanosaude) {
    Pacienteplanosaude.on('dataSourceAttached', function(obj) {
        Pacienteplanosaude.custom.autocomplete.planodesaude = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.planodesaude.nome;
            }
        };
    });
}