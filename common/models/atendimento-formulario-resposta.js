'use strict';

module.exports = function(Atendimentoformularioresposta) {
    Atendimentoformularioresposta.on('dataSourceAttached', function(obj) {
        Atendimentoformularioresposta.custom.autocomplete.atendimento = {
            where: function(reg) {
                return {
                    id: (reg),
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.id
                };
                return newRow;
            },
            label: function(row) {
                return row.id;
            }
        };

        Atendimentoformularioresposta.custom.autocomplete.formulario = {
            where: function(reg) {
                return {
                    nome: (reg),
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.formulario.nome;
            }
        };

        Atendimentoformularioresposta.custom.autocomplete.formulario_indicador = {
            where: function(reg) {
                return {
                    nome: (reg),
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.formulario_indicador.nome;
            }
        };

    });
};
