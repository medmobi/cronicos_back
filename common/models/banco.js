'use strict';

module.exports = function (Banco) {

    Banco.populatebancos = function (req, fn) {
        var bancoList = req;

        var tango;
        bancoList.forEach(function (banco) {
            if (banco.banco_code) {
                tango = new Banco(banco);
                Banco.create(tango, function (err, res) {
                    if (err) {
                        console.log(err);
                    } else {
                        //console.log(res);
                    }
                })
            } else {
                //console.log(banco);
            }
        })

        fn();
    }

    Banco.remoteMethod(
        'populatebancos',
        {
            description: 'Populates the bancoList',
            accepts: [
                { arg: 'req', type: 'object', required: true, http: { source: 'body' } }
            ],
            http: { verb: 'post', path: '/populate-bancos' }
        }
    )
};
