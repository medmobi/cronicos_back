'use strict';

var passport = require('passport');
var g = require('strong-globalize')();
var emailSender = require('../../utils/email-sender');
var loopback = require('loopback');
var path = require('path');

module.exports = function (Usuario) {

    Usuario.userLogout = userLogout;
    Usuario.remoteMethod('userLogout', {
        description: 'Logs user out.',
        accepts: [{
            arg: 'req',
            type: 'object',
            required: true,
            http: {
                source: 'body'
            }
        },
            // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
        ],
        http: {
            verb: 'post',
            path: '/user-logout'
        }
    });

    Usuario.resetPasswordAction = resetPasswordAction;
    Usuario.remoteMethod('resetPasswordAction', {
        description: 'Action to reset user password',
        accepts: [{
            arg: 'data',
            type: 'object',
            required: true,
            http: {
                source: 'body'
            }
        },],
        http: {
            verb: 'post',
            path: '/reset-password'
        }
    });

    Usuario.getMenu = getMenu;
    Usuario.remoteMethod('getMenu', {
        description: 'Get Menu da permissao com base no usuario',
        accepts: [{
            arg: 'accessToken',
            type: 'any',
            required: true,
            http: { source: 'path' }
        },
            // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
        ],
        http: {
            verb: 'get',
            path: '/:accessToken/user-menu'
        }
    });

    Usuario.changePwd = changePwd;
    Usuario.remoteMethod('changePwd', {
        description: 'Changes a logged in user password.',
        accepts: [{
            arg: 'req',
            type: 'object',
            required: true,
            http: {
                source: 'body'
            }
        },
        {
            arg: 'accessToken',
            type: 'object',
            http: function (ctx) {
                if (!ctx.req.accessToken) {
                    var createErr = new Error(g.f("Access Token is required"));
                    createErr.statusCode = 401;
                    createErr.name = 'NOT_AUTHORIZED';
                    return createErr;
                } else {
                    return ctx.req.accessToken;
                }
            }
        }
        ],
        http: {
            verb: 'post',
            path: '/change-pwd'
        }
    });

    Usuario.sendDocumentEmail = sendDocumentEmail;
    Usuario.remoteMethod('sendDocumentEmail', {
      description: 'Send a document by email to user.',
      accepts: [
        {
          arg: 'req',
          type: 'object',
          required: true,
          http: {
            source: 'body'
          }
        },
        {
          arg: 'accessToken',
          type: 'object',
          http: function (ctx) {
            if (!ctx.req.accessToken) {
              var createErr = new Error(g.f("Access Token is required"));
              createErr.statusCode = 401;
              createErr.name = 'NOT_AUTHORIZED';
              return createErr;
            } else {
              return ctx.req.accessToken;
            }
          }
        }
      ],
      http: {
        verb: 'post',
        path: '/send-document-email'
      },
      returns: { arg: 'res', type: 'object', root: true }
    });

    function userLogout(req, fn) {
        console.log('LOGOUT!');
        if (!req.accessToken) {
            var err = new Error(g.f('Token required'));
            err.statusCode = 401;
            err.name = 'LOGOUT_FAILED';
            fn(err);
        } else {
            Usuario.logout(req.accessToken, function (err) {
                if (err) {
                    console.log('Erro no logout');
                    fn(err);
                } else {
                    console.log('Sucesso no Logout');
                    fn(null);
                }
            })
        }
    }
    function changePwd(req, accessToken, fn) {
        if (accessToken instanceof Error) {
            var err = accessToken;
            fn(err);
        }

        // Validations
        console.log(req);
        if (!(req.password_old && req.password_new && req.password_new_confirmation)) {
            var missingAttributes = new Error(g.f("Missing attributes: old password (oldPwd), new password (newPwd) and confirmation (confPwd)"));
            missingAttributes.statusCode = 422;
            fn(missingAttributes);
        }

        if (!req.password_new === req.password_new_confirmation) {
            var notMatchedPwd = new Error(g.f("New password and confirmation do not match"));
            notMatchedPwd.statusCode = 422;
            fn(notMatchedPwd);
        }
        // End validations
        console.log(accessToken);

        Usuario.findById(accessToken.userId, function (err, user) {
            if (err) {
                console.log('An error is reported from User.findById: %j', err);
                fn(err);
            }

            console.log(user);

            if (!user) {
                var createErr = new Error(g.f("User does not exist"));
                createErr.statusCode = 404;
                createErr.name = 'USER_NOT_FOUND';
                fn(createErr);
            }

            if (user) {
                // Verify if the old pwd is correct
                user.hasPassword(req.password_old, function (err, isMatch) {
                    if (err) {
                        fn(err);
                    } else {
                        // Verify if the new Pwd is different than the old one
                        user.hasPassword(req.password_new, function (err, isMatch) {
                            if (err) {
                                console.log(err)
                            }
                            if (isMatch) {
                                var samePwd = new Error(g.f("The new password needs to be different than the old one"));
                                samePwd.statusCode = 422;
                                samePwd.name = 'SAME_PWD';
                                fn(samePwd);
                            }
                        })

                        if (isMatch) {
                            user.password = Usuario.hashPassword(req.password_new);
                            user.save(function (err, user) {
                                if (err) {
                                    console.log(err);
                                    var changePwdFailed = new Error(g.f("Could not change password"));
                                    changePwdFailed.statusCode = 422;
                                    fn(changePwdFailed);
                                } else {
                                    console.log('Senha alterada!');
                                    fn(null);
                                }
                            })
                        } else {
                            var wrongPwd = new Error(g.f("The old password is wrong"));
                            wrongPwd.statusCode = 422;
                            wrongPwd.name = 'WRONG_OLD_PWD';
                            fn(wrongPwd);
                        }

                    }
                })
            }
        })
    }
    function getMenu(accessToken, fn) {
        console.log(accessToken);
        if (!accessToken) {
            var err = new Error(g.f('Token required'));
            err.statusCode = 401;
            err.name = 'GETMENU_FAILED';
            fn(err);
        } else {

            console.log('getMenu');
            console.log(Usuario.PerfilControleAcesso);
        }
    }
    function resetPasswordAction(data, fn) {
        // var accessToken = , password, confirmation
        // console.log(data);
        fn = fn || utils.createPromiseCallback();
        if (!data.accessToken) {
            var accErr = new Error(g.f("Missing AccessToken"));
            accErr.statusCode = 401;
            fn(accErr);
        }

        //verify passwords match
        if (!data.password ||
            !data.confirmation ||
            data.password !== data.confirmation) {
            // res.sendStatus(400, );
            var passErr = new Error('Passwords do not match');
            passErr.statusCode = 422;

            fn(passErr);
        }

        // console.log(data.accessToken);

        Usuario.app.models.AccessToken.findOne({
            where: {
                id: data.accessToken
            }
        }, function(err, token) {
            if (err) {
                console.log(err)
                var accErr = new Error(g.f("Invalid AccessToken"));
                accErr.statusCode = 401;
                fn(accErr);
            } else {
                if (token) {
                    Usuario.findOne({
                        where: {
                            'id': token.userId
                            // 'resetPasswordToken.id': data.accessToken
                        }
                    }, function (err, user) {
                        if (err) {
                            console.log(err)
                            var accErr = new Error(g.f("Invalid AccessToken"));
                            accErr.statusCode = 401;
                            fn(accErr);
                        } else {
                            if (user) {
                                var expiryDate = new Date(token.created).getTime() + token.ttl * 1000;
                                // var expiryDate = new Date(user.resetPasswordToken.created).getTime() + user.resetPasswordToken.ttl * 1000;

                                console.log('NOW');
                                console.log(new Date());
                                console.log('Expiry date');
                                console.log(new Date(expiryDate));

                                if (Date.now() > expiryDate) {
                                    console.log('Token Expirado');
                                    var expiredToken = new Error(g.f("The token has expired"));
                                    expiredToken.statusCode = 401;
                                    fn(expiredToken);
                                } else {
                                    user.password = Usuario.hashPassword(data.password);
                                    user.resetPasswordToken = null;
                                    user.resetPassword = true;
                                    user.save(function (err) {
                                        if (err) {
                                            fn(err);
                                        } else {
                                            console.log('> password reset processed successfully');
                                            fn();
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });

        return fn.promise;
    }
    function _findRole(Role, type) {
        return Role.findOne({
            where: {
                name: type
            }
        });
    }
    function _saveRoleMapping(id, type, role, models) {
        return new Promise(function (resolve, reject) {
            role.principals.create({
                principalType: models.RoleMapping.USER,
                principalId: id
            })
            .then(function (roleMap) {
                if (!roleMap) reject("Role mapping for user " + type + " with ID " + id + "wasn't possible!");
                else {
                    console.log("Role mapping created for user " + type + " with ID " + id + "!");
                    resolve(roleMap);
                }
            })
        });
    }
    function _checkRoleMappingExistence(id, type, models) {
        var _role = null;
        return _findRole(models.Role, type)
          .then(function (searchedRole) {
              if (!searchedRole) throw "No role found for " + type + "!";

              _role = searchedRole;
              return models.RoleMapping.find(
                  { where: {
                      and: [
                          { roleId: _role.id },
                          { principalId: id }
                      ]
                  }}
              );
          })
          .then(function (roleMap) {
              if (roleMap.length) throw "Role mapping found for user type " + type + " with ID " + id + "!";

              return _saveRoleMapping(id, type, _role, models);
          });
    }
    function checkSaveRoleMapping(user, models) {
        var _roles = new Array();

        if (user.admin) _roles.push('admin');
        if (user.pres_id) _roles.push('prestador');

        if (!_roles.length) console.log('User not an admin or prestador. Not saving role mapping!');
        else {
            var _rolesPromise = new Array();
            _roles.forEach(function (type) {
                _rolesPromise.push(_checkRoleMappingExistence(user.id, type, models));
            });

            Promise.all(_rolesPromise)
                .then(function () {
                    console.log('Role mapping executed for user: ', user.nome);
                })
                .catch(function error(err) {
                    console.log(err);
                });
        }
    }
    function deleteRoleMapping(id, RoleMapping) {
        return RoleMapping.find({ where: { principalId: id }})
          .then(function (roleMappings) {
              var _roleMapPromises = new Array();
              roleMappings.forEach(function (roleMap) {
                  _roleMapPromises.push(RoleMapping.deleteById(roleMap.id));
              });

              if (_roleMapPromises.length) return Promise.all(_roleMapPromises);
              else throw "No role mapping found for this user ID " + id + "!";
          });
    }
    function sendEmailOnCreate(user) {
        var Config = Usuario.app.models.Config;
        var header_url = Config.get('FRONTEND_URL') + '/assets/images/main-logo.png';

        //use info for create options

        var options = {
            name: user.nome,
            password: user.confirm_password,
            email: user.email,
            header_url: header_url
        };
        var template = path.resolve(__dirname, '../../server/views/send-new-user.ejs');
        var template = loopback.template(template);
        var html = template(options);


        var user_create = {
            to: user.email,
            from: 'MedMobi Crônicos' + ' <' + Config.get('email_sender') + '>',
            subject: 'Dados da criação do Usuário',
            html: html
        };
        emailSender.sendEmail(user_create);
    }
    function sendDocumentEmail (body, accessToken, fn) {
        var Config = Usuario.app.models.Config;
        var header_url = Config.get('FRONTEND_URL') + '/assets/images/main-logo.png';
        var empresa_logo_url = Config.get('FRONTEND_URL') + '/upload/logos/download/' + body.empresa.logo;
        //use info for create options

        function convertDate (date) {
            var monthNames = [
                "Janeiro", "Fevereiro", "Março",
                "Abril", "Maio", "Junho", "Julho",
                "Agosto", "Setembro", "Outubro",
                "Novembro", "Dezembro"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' de ' + monthNames[monthIndex] + ' de ' + year;
        }

        var options = {
          paciente: body.paciente,
          prestador: body.prestador,
          empresa: body.empresa,
          empresa_logo_url: empresa_logo_url
        };

        var emailTitle = '';

        if (body.type === 'receituario') {
            options.receituario = body.receituario;
            options.receituarioItems = body.receituario.receituario_itens;
            var genderTxt = 'Dr(a). ';
            if (body.prestador.sexo === 'f') genderTxt = 'Dra. ';
            else if (body.prestador.sexo === 'm') genderTxt = 'Dr. ';
            emailTitle = 'Prescrição de Receita - ' + genderTxt + body.prestador.nome;

            options.genderTxt = genderTxt;
            options.date = convertDate(new Date(body.receituario.data));
        }
        else throw new Error('This type of document was not prepared to be sent by email!');

        var template = path.resolve(__dirname, '../../server/views/'+body.type+'.ejs');
        var template = loopback.template(template);
        var html = template(options);

        var prescription_created = {
          to: body.paciente.email,
          from: 'MedMobi Crônicos' + ' <' + Config.get('email_sender') + '>',
          subject: emailTitle,
          html: html
        };

        emailSender.sendEmail(prescription_created)
            .then(function (res) {
                fn(null, res);
            })
            .catch(function (err) {
                fn(err);
            });
    }

    Usuario.on('resetPasswordRequest', function (info) {
        const Config = Usuario.app.models.Config;
        var url = Config.get('FRONTEND_URL') + '/nova-senha' + '?access_token=' + info.accessToken.id;
        var header_url = Config.get('FRONTEND_URL') + '/assets/images/main-logo.png';
        var user = info.user;
        user.resetPassword = true;

        var options = {
            user: user,
            url: url,
            header_url: header_url
        };

        var template = path.resolve(__dirname, '../../server/views/reset-password.ejs');
        var template = loopback.template(template);
        var html = template(options);

        // console.log(info);
        user.resetPasswordToken = info.accessToken;

        user.save(function (err) {
            if (err) {
                // fn(err);
                //@todo user response not defined
                console.log('//@todo user response not defined.');
            } else {
                var password_recover = {
                    to: info.email,
                    from: Config.get('email_name') + ' <' + Config.get('email_sender') + '>',
                    subject: 'Recuperação de Senha',
                    html: html
                }
                emailSender.sendEmail(password_recover);
            }
        });
    });
    Usuario.on('dataSourceAttached', function (obj) {
        // console.log('tango telefone');
        Usuario.custom.autocomplete.perfil_usuario = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.perfil_usuario.nome;
            }
        };

        Usuario.custom.autocomplete.empresa = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.empresa.nome;
            }
        };

        Usuario.custom.autocomplete.prestador = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.prestador.nome;
            }
        };
    });

    // After creating user, send an email for him and create role maps
    Usuario.observe('after save', function(ctx, next) {
        if (ctx.instance) {
            if (!ctx.instance.resetPassword) sendEmailOnCreate(ctx.instance);
            checkSaveRoleMapping(ctx.instance, ctx.Model.app.models);
        }
        return next();
    });

    Usuario.observe('before delete', function(ctx, next) {
        if (ctx.where) {
            deleteRoleMapping(ctx.where.id.inq[0], ctx.Model.app.models.RoleMapping)
                .then(function success(count) {
                    console.log('Removed ' + count.length + ' role mappings for user ID ' + ctx.where.id.inq[0] + '!');
                    return next();
                })
                .catch(function error(err) {
                    console.log('Error: ', err);
                    return next();
                });
        }
    });
};
