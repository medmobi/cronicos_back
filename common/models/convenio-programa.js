'use strict';

module.exports = function(Convenioprograma) {
    Convenioprograma.on('dataSourceAttached', function(obj) {
        Convenioprograma.custom.autocomplete.programa = {
            where: function(reg) {
                return {
                    descricao: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.descricao
                };
                return newRow;
            },
            label: function(row) {

                return row.programa.descricao;
            }
        };
    });
};