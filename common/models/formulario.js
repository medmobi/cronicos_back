'use strict';

module.exports = function(Formulario) {
  Formulario.validatesUniquenessOf('nome', {message: 'Formulário já existente!'});
  Formulario.filterByGeralAndEspecialidades = filterByGeralAndEspecialidades;
  Formulario.remoteMethod('filterByGeralAndEspecialidades', {
    description: 'Retrieves atendimento formularios with prestador especialidades list',
    accepts: [{
        arg: 'req',
        type: 'object',
        description: 'Model id',
        required: true,
        http: { source: 'body' }
      }],
    returns: { arg: 'formularios', type: 'array' },
    http: { verb: 'post', path: '/filtered' }
  });

  function filterByGeralAndEspecialidades(req, fn) {
    console.log('get filtered formularios!');

    Formulario.find({
      include: [
        { 'indicadores': { 'indicador': ['tipo', 'unidade_medida'] } },
        { 'especialidades': { 'especialidade': [] } }
      ]
    }, function (err, rows) {
      if (err) {
        fn(err);
      }
      else {
        var _rows = [];

        rows.forEach(function (row) {
          var added = false;
          var especialidades = row.especialidades();
          if (especialidades !== undefined) {
            especialidades.forEach(function (esp) {
              var espExists = req.especialidades.some(function (el) {
                return el.id === esp.esp_id;
              });
              if (espExists) {
                if (req.autocomplete) row.label = row.nome;
                _rows.push(row);
                added = true;
                return;
              }
            });
            if (row.geral && !added) {
                if (req.autocomplete) row.label = row.nome;
                _rows.push(row);
            }
          }
        });

        fn(null, _rows);
      }
    });
  }
};
