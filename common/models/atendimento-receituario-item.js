'use strict';

module.exports = function (Atendimentoreceituarioitem) {
    Atendimentoreceituarioitem.on('dataSourceAttached', function (obj) {

        Atendimentoreceituarioitem.custom.autocomplete.medicamento = {
            where: function (reg) {
                return {
                    nome_apresentacao: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome_apresentacao
                };
                return newRow;
            },
            label: function (row) {
                return row.medicamento.nome_apresentacao;
            }
        };

        Atendimentoreceituarioitem.custom.autocomplete.posologia = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.posologia.nome;
            }
        };

        Atendimentoreceituarioitem.custom.autocomplete.dosagem = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.dosagem.nome;
            }
        };

        Atendimentoreceituarioitem.custom.autocomplete.unidade_medida = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.unidade_medida.nome;
            }
        };

        Atendimentoreceituarioitem.custom.autocomplete.via = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.via.nome;
            }
        };

        Atendimentoreceituarioitem.custom.autocomplete.duracao = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.duracao.nome;
            }
        };

        Atendimentoreceituarioitem.custom.autocomplete.frequencia = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.frequencia.nome;
            }
        };
    });
};
