'use strict';

module.exports = function(Especialidade) {
    Especialidade.validatesUniquenessOf('descricao', {message: 'Especialidade já existente!'});
};
