'use strict';

module.exports = function(Pacienteexamedigital) {
    Pacienteexamedigital.on('dataSourceAttached', function(obj) {
        Pacienteexamedigital.custom.autocomplete.indicador = {
            include: ['resultados', 'tipo'],
            where: function(reg) {
                return {
                    descricao: (reg),
                    exame: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.codigo + ' - ' + row.descricao,
                    resultados: row.resultados(),
                    tipo: row.tipo(),
                    formula: row.formula
                };

                return newRow;
            },
            label: function(row) {
                return row.indicador.descricao;
            }
        };

    });
};
