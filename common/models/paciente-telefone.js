'use strict';

module.exports = function(Pacientetelefone) {
    Pacientetelefone.on('dataSourceAttached', function(obj) {
        // console.log('tango telefone');
        Pacientetelefone.custom.autocomplete.tipo_telefone = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.tipo_telefone.nome;
            }
        };
    });
};