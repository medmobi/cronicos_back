'use strict';

module.exports = function(Pacientedocumentomedico) {
  Pacientedocumentomedico.on('dataSourceAttached', function(obj) {
    Pacientedocumentomedico.custom.autocomplete.prestador = {
      where: function(reg) {
        return {
          nome: (reg),
          situacao: true
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function(row) {
        return row.prestador.nome;
      }
    };
    Pacientedocumentomedico.custom.autocomplete.documento_tipo = {
      where: function(reg) {
        return {
          nome: (reg)
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function(row) {
        return row.documento_tipo.nome;
      }
    };
  });
};
