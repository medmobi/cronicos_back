module.exports = function() {

  const production = [

    {
      "key": "email_sender",
      "value": "contato@greatmob.com.br",
      "public": false
    },
    {
      "key": "email_name",
      "value": "MedMobi Crônicos",
      "public": false
    },
    {
      "key": "email_name_adm",
      "value": "Crônicos",
      "public": false
    },
    {
      "key": "email_sender_adm",
      "value": "contato@greatmob.com.br",
      "public": false
    },
    {
      "key": "emails_adms",
      "value": ["contato@greatmob.com.br"],
      "public": false
    },
    {
      "key": "CONFIG_SCHEDULE",
      "value": "*/5 * * * *",
      "public": false
    },
    {
      "key": "DDDBR",
      "value": "+55",
      "public": false
    },
    {
      "key": "BACKEND_URL",
      "value": "api.cronicos.anexs.com.br",
      "public": false
    },
    {
      "key": "PROTOCOL",
      "value": "http",
      "public": false
    },
    {
      "key": "MAX_PAYMENT",
      "value": 100000,
      "public": false
    },
    {
      "key": "FRONTEND_URL",
      "value": "http://cronicos.anexs.com.br",
      "public": false
    }
  ];

  return production;

  // const environment = process.env.NODE_ENV;

  // if (environment === 'staging') {
  // 	return staging;
  // } else if (environment === 'testing') {
  // 	return testing;
  // } else if (environment === 'production') {
  // 	return production;
  // } else {
  // 	return local;
  // }
}
