'use strict'

var fs = require('fs');
var path = require('path');

var app = require(path.resolve(__dirname, '../../server/server'));

var modelConfigFile = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../../server/model-config.json'), 'utf8'));

var menus = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../raw/menus.json'), 'utf8'));
var menusConverted = new Array();

var paises = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../raw/paises.json'), 'utf8'));
var paisesConverted = new Array();

var cidadesEstados = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../raw/cidadesEstado.json'), 'utf8'));
var cidadesConverted = new Array();

var tipoMedicamentos = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../raw/medicamento-tipos.json'), 'utf8'));
var tipoMedicamentosConverted = new Array();

var medicamentos = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../raw/medicamentos.json'), 'utf8'));
var medicamentosConverted = new Array();

function readExportModelsJSON() {
    var models = new Array();
    var notDBmodels = ['transient', 'file', 'sendgrid'];
    Object.keys(modelConfigFile).forEach(function (model) {
        if (model !== '_meta') {
            if (notDBmodels.indexOf(modelConfigFile[model].dataSource) === -1) {
                if (!modelConfigFile[model].table) {
                    models.push(model);
                }
            }
        }
    });

    fs.writeFile(path.resolve(__dirname, '../../server/data/application.models.json'), JSON.stringify(models, null, 4), function (err) {
        if (err) {
            console.error('Error in exporting application models: ', err);
        } else {
            console.log('Models exported to JSON with success!');
        }
    });
}
function readExportMenusJSON() {
    var idCount = 1;
    menus.forEach(function (_row) {
        var paiId = idCount;
        var __row = {"id": idCount, "nome": _row.men_name, "icone": _row.men_icon};
        menusConverted.push(__row);

        idCount++;

        _row.items.forEach(function(_item) {
            var __rowItem = {
                "id": idCount,
                "nome": _item.men_name,
                "settings": _item.men_route.replace('app.', ''),
                "pai": paiId,
                "icone": (_item.men_icon == undefined ? null : _item.men_icon)
            };

            menusConverted.push(__rowItem);
            idCount++;
        });
    });

    fs.writeFile(path.resolve(__dirname, '../data/menu.json'), JSON.stringify(menusConverted, null, 4), function (err) {
        if (err) {
            console.error('Error in exporting application menus: ', err);
        } else {
            console.log('Menus exported to JSON with success!');
        }
    });
}
function readExportPaisesJSON() {
    var idCount = 1;
    paises.forEach(function (_paisName) {
        var _row = { "id": idCount, "nome": _paisName };
        idCount++;
        paisesConverted.push(_row);
    });

    fs.writeFile(path.resolve(__dirname, '../data/pais.json'), JSON.stringify(paisesConverted, null, 4), function (err) {
        if (err) {
            console.error('Error in exporting application paises: ', err);
        } else {
            console.log('Paises exported to JSON with success!');
        }
    });
}
function readExportCidadesJSON() {
    app.models.Estado.find()
        .then(function (estadosFromDB) {
            cidadesEstados.forEach(function (estado) {
                estadosFromDB.forEach(function (estadoFromDB) {
                    if (estado.nome === estadoFromDB.nome) {
                        estado.cidades.forEach(function (cidade) {
                            var _row = { "nome": cidade, "estado_id": estadoFromDB.id };
                            cidadesConverted.push(_row);
                        });
                        return;
                    }
                });
            });

            fs.writeFile(path.resolve(__dirname, '../data/cidade.json'), JSON.stringify(cidadesConverted, null, 4), function (err) {
                if (err) {
                    console.error('Error in exporting application cidades: ', err);
                } else {
                    console.log('Cidades exported to JSON with success!');
                }
            });
        });


  //

}
function readExportTipoMedicamentosJSON() {
    var idCount = 1;
    tipoMedicamentos.forEach(function (_tipo) {
        var _row = { "id": idCount, "nome": _tipo };
        idCount++;
        tipoMedicamentosConverted.push(_row);
    });

    return new Promise(function (resolve, reject) {
        fs.writeFile(path.resolve(__dirname, '../data/tipo-medicamento.json'), JSON.stringify(tipoMedicamentosConverted, null, 4), function (err) {
            if (err) {
                console.error('Error in exporting application tipo de medicamentos: ', err);
                reject(err);
            } else {
                console.log('Tipo de medicamentos exported to JSON with success!');
                resolve(tipoMedicamentosConverted);
            }
        });
    });
}
function readExportMedicamentosJSON() {
  readExportTipoMedicamentosJSON()
    .then(function success(tipoMedicamentosConverted) {
        var idCount = 1;
        medicamentos.forEach(function (_med) {
            var _tipo = tipoMedicamentosConverted.find(function (el) {
                if(el.nome === _med.tipo) return el;
            });
            var _row = { "id": idCount, "nome": _med.nome, "tipo_id": _tipo.id };
            idCount++;
            medicamentosConverted.push(_row);
        });

        fs.writeFile(path.resolve(__dirname, '../data/medicamento.json'), JSON.stringify(medicamentosConverted, null, 4), function (err) {
            if (err) {
                console.error('Error in exporting application medicamentos: ', err);
            } else {
                console.log('Medicamentos exported to JSON with success!');
            }
        });
    })
    .catch(function error(err) {
        console.error('Error in exporting application tipo de medicamentos: ', err);
    });
}

module.exports = {
    readExportModelsJSON: readExportModelsJSON,
    readExportMenusJSON: readExportMenusJSON,
    readExportPaisesJSON: readExportPaisesJSON,
    readExportCidadesJSON: readExportCidadesJSON,
    readExportTipoMedicamentosJSON: readExportTipoMedicamentosJSON,
    readExportMedicamentosJSON: readExportMedicamentosJSON
};
