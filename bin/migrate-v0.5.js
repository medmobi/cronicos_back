var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.postgres;

// var migrateList = ['ConveniadoTelefone','EmpresaTelefone','PacienteTelefone','PrestadorTelefone'];
// var migrateList = ['Especialidade', 'Programa', 'Doenca', 'CategoriaDoenca', 'Feriado', 'UnidadeMedida', 'Banco', 'PlanodeSaude', 'EstadoCivil', 'TipoSanguineo', 'Pais', 'Estado', 'Cidade', 'Posologia', 'Via', 'Duracao', 'Frequencia', 'PerfilUsuario', 'Paciente', 'PacienteEndereco', 'PacienteTelefone', 'Conveniado', 'ConveniadoEndereco', 'ConveniadoTelefone', 'Prestador', 'PrestadorEndereco', 'PrestadorTelefone', 'PacientePrograma', 'PrestadorEspecialidade', 'PrestadorBanco', 'ConveniadoContato', 'Convenio', 'ConvenioPrograma', 'PacientePlanoSaude', 'EmpresaEndereco', 'EmpresaTelefone', 'Conselho', 'Indicador', 'IndicadorTabelaResultado', 'Formulario', 'FormularioIndicador', 'ControleAcesso', 'PerfilControleAcesso'];
  var migrateList = ['Formulario', 'AtendimentoReceituario', 'AtendimentoReceituarioItem', 'Usuario', 'FormularioEspecialidade', 'PrestadorEspecialidade', 'StatusAtendimento', 'Atendimento', 'AtendimentoEncaminhamento', 'PreReceita', 'PreReceitaItem', 'PacienteExame', 'Paciente'];

migrateList.forEach(function(model) {
    ds.autoupdate(model, function(err) {
        if (err) throw err;
    });
})
