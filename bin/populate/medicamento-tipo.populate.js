var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var tiposMedicamento = require(path.resolve(__dirname, '../data_medicamentos/tipo-medicamento.json'));

tiposMedicamento.forEach(function(_row) {
  app.models.TipoMedicamento.create(_row, function(err, model) {
    if (err) throw err;
    console.log('Created: ', model);
  });
});
