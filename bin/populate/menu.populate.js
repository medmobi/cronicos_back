var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var menus = require(path.resolve(__dirname, '../raw/menu.json'));

var count = menus.length;
menus.forEach(function (_row) {
  var __row = {"nome": _row.men_name, "icone": _row.men_icon};

  app.models.Menu.create(__row, function (err, model) {
    if (err) throw err;

    console.log('Created:', model);

    _row.items.forEach(function(_item) {
      var __rowItem = {
        "nome": _item.men_name,
        "settings": _item.men_route.replace('app.', ''),
        "pai": model.id,
        "icone": (_item.men_icon == undefined ? null : _item.men_icon)
      };

      app.models.Menu.create(__rowItem, function (err, modelItem) {
        if (err) throw err;

        console.log('Created: ', modelItem);
      });
    });

    count--;
  });
});
