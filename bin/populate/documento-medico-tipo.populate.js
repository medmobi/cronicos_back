var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var documentoTipos = require(path.resolve(__dirname, '../data/documento-medico-tipo.json'));

var count = documentoTipos.length;

documentoTipos.forEach(function (_row) {
  app.models.DocumentoMedicoTipo.create(_row, function (err, model) {
    if (err) throw err;

    console.log('Created: ', model);

    count--;
  });
});
