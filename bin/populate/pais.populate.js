var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var paises = require(path.resolve(__dirname, '../data/pais.json'));

var pais = app.models.Pais;

var count = paises.length;
paises.forEach(function (_row) {
    var __row = { "Nome": _row };
    app.models.Pais.create(__row, function (err, model) {
        if (err) throw err;

        console.log('Created: ', model);

        count--;
    });
});
