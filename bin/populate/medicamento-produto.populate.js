var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var produtosMedicamento = require(path.resolve(__dirname, '../data_medicamentos/produto-medicamento.json'));

produtosMedicamento.forEach(function(_row) {
  app.models.ProdutoMedicamento.create(_row, function(err, model) {
    if (err) throw err;

    console.log('Created: ', model);
  });
});
