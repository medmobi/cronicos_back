var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var tarjasMedicamento = require(path.resolve(__dirname, '../data_medicamentos/tarja-medicamento.json'));

tarjasMedicamento.forEach(function(_row) {
  app.models.TarjaMedicamento.create(_row, function(err, model) {
    if (err) throw err;
    console.log('Created: ', model);
  });
});
