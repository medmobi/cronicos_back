var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.postgres;

// 1. Staging Release [02/2018]
// var updateList = ['Especialidade', 'Programa', 'Doenca', 'CategoriaDoenca', 'Feriado', 'UnidadeMedida', 'Banco', 'PlanodeSaude', 'EstadoCivil', 'TipoSanguineo', 'Pais', 'Estado', 'Cidade', 'Posologia', 'Via', 'Duracao', 'Frequencia', 'PerfilUsuario', 'Paciente', 'PacienteEndereco', 'PacienteTelefone', 'Conveniado', 'ConveniadoEndereco', 'ConveniadoTelefone', 'Prestador', 'PrestadorEndereco', 'PrestadorTelefone', 'PacientePrograma', 'PrestadorEspecialidade', 'PrestadorBanco', 'ConveniadoContato', 'Convenio', 'ConvenioPrograma', 'PacientePlanoSaude', 'EmpresaEndereco', 'EmpresaTelefone', 'Conselho', 'Indicador', 'IndicadorTabelaResultado', 'Formulario', 'FormularioIndicador', 'ControleAcesso', 'PerfilControleAcesso'];

// 2. Dev Release [28/02/2018]
// var updateList = ['ConveniadoTelefone','EmpresaTelefone','PacienteTelefone','PrestadorTelefone'];

// 3. Staging Release [19/03/2018]
// var updateList = ['Formulario', 'AtendimentoReceituarioItem', 'Usuario', 'FormularioEspecialidade', 'PrestadorEspecialidade', 'StatusAtendimento', 'Atendimento', 'AtendimentoEncaminhamento', 'PreReceita', 'PreReceitaItem', 'PacienteExame', 'Paciente'];

// 4. Staging Release [25/03/2018] - laudo removed from exame
// var updateList = ['PacienteExame', 'Paciente', 'AtendimentoReceituarioItem', 'AtendimentoReceituario', 'Atendimento', 'PreReceitaItem', 'PreReceita'];

// 5. Staging Release [01/04/2018] - Atestados, Update medicamentos
// var updateList = ['DocumentoMedico', 'DocumentoMedicoTipo', 'PacienteDocumentoMedico', 'Paciente','TipoMedicamento', 'Medicamento'];

// 6. Staging Release [09/04/2018] - ACL, Role, RoleMapping
// var updateList = ['ACL', 'Role', 'RoleMapping'];

// 7. Staging Release [16/04/2018] - Increment medicamentos
// var updateList = ['TipoMedicamento', 'TarjaMedicamento', 'ProdutoMedicamento', 'ClasseTerapeuticaMedicamento', 'LaboratorioMedicamento', 'PrincipioAtivoMedicamento', 'Medicamento'];

// 8. Staging Release [23/04/2018] - Agenda, IndicadorTabelaResultado, PacienteExameDigital
// var updateList = ['Indicador','IndicadorTabelaResultado', 'PacienteExameDigital', 'TipoEvento', 'StatusEvento', 'Evento'];

// 9. Staging Release [24/04/2018] - PerfilControleAcesso, PerfilUsuario
// var updateList = ['PerfilControleAcesso','PerfilUsuario'];

// 10. Staging Release [14/05/2018] - Empresa
// var updateList = ['Empresa'];

// 11. Staging Release [21/05/2018] - PacienteExameDigital, Pessoa, Indicador, IndicadorCid
// var updateList = ['PacienteExameDigital', 'Pessoa', 'Indicador', 'IndicadorCid'];

// 12. Staging Release [28/05/2018] - Config (w/ data)
// var updateList = ['Config'];

// 13. Staging Release [04/06/2018] - Evento, Menu (only data), StatusEvento (only data), AtendimentoReceituario
var updateList = ['Evento', 'Menu', 'StatusEvento', 'AtendimentoReceituario'];

updateList.forEach(function(model) {
  ds.autoupdate(model, function(err) {
    if (err) throw err;
  });
});
