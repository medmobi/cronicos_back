var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.postgres;

var migrateList = ['Indicador'];
// var migrateList = ['Especialidade', 'Programa', 'Doenca', 'CategoriaDoenca', 'Feriado', 'UnidadeMedida', 'Banco', 'PlanodeSaude', 'EstadoCivil', 'TipoSanguineo', 'Pais', 'Estado', 'Cidade', 'Posologia', 'Via', 'Duracao', 'Frequencia', 'PerfilUsuario', 'Paciente', 'PacienteEndereco', 'PacienteTelefone', 'Conveniado', 'ConveniadoEndereco', 'ConveniadoTelefone', 'Prestador', 'PrestadorEndereco', 'PrestadorTelefone', 'PacientePrograma', 'PrestadorEspecialidade', 'PrestadorBanco', 'ConveniadoContato', 'Convenio', 'ConvenioPrograma', 'PacientePlanoSaude', 'EmpresaEndereco', 'EmpresaTelefone', 'Conselho', 'Indicador', 'IndicadorTabelaResultado', 'Formulario', 'FormularioIndicador', 'ControleAcesso', 'PerfilControleAcesso'];

migrateList.forEach(function(model) {
    ds.automigrate(model, function(err) {
        if (err) throw err;
    });
})

// ds.automigrate('TipodeTelefone', function(err) {
//     if (err) throw err;

//     var rows = [{
//             Nome: 'Comercial'
//         },
//         {
//             Nome: 'Residencial'
//         },
//         {
//             Nome: 'Celular'
//         }, {
//             Nome: 'Recado'
//         }
//     ];
//     var count = rows.length;
//     rows.forEach(function(_row) {
//         app.models.TipodeTelefone.create(_row, function(err, model) {
//             if (err) throw err;

//             console.log('Created:', model);

//             count--;
//             if (count === 0)
//                 ds.disconnect();
//         });
//     });
// });

// ds.automigrate('TipoFeriado', function(err) {
//     if (err) throw err;

//     var rows = [{
//             nome: 'Nacional'
//         },
//         {
//             nome: 'Estadual'
//         },
//         {
//             nome: 'Municipal'
//         }, {
//             nome: 'Mundial'
//         }
//     ];
//     var count = rows.length;
//     rows.forEach(function(_row) {
//         app.models.TipoFeriado.create(_row, function(err, model) {
//             if (err) throw err;

//             console.log('Created:', model);

//             count--;
//             if (count === 0)
//                 ds.disconnect();
//         });
//     });
// });

// ds.automigrate('AccessToken', function(err) {
//     if (err) throw err;
// });




// ds.automigrate('Empresa', function(err) {
//     if (err) throw err;

//     var empresa = [{
//         id: 1,
//         nome: 'Empresa teste'
//     }];
//     var count = users.length;
//     empresa.forEach(function(user) {
//         app.models.Empresa.create(empresa, function(err, model) {
//             if (err) throw err;

//             console.log('Created:', model);

//             count--;
//             if (count === 0)
//                 ds.disconnect();
//         });
//     });
// });

// ds.automigrate('IndicadorTipo', function(err) {
//     if (err) throw err;

//     var rows = [{
//             nome: 'Numérico'
//         },
//         {
//             nome: 'Texto'
//         },
//         {
//             nome: 'Resultado'
//         }, {
//             nome: 'Alternativa S/N'
//         }, {
//             nome: 'Lista'
//         }, {
//             nome: 'Multiplas Alternativas'
//         }
//     ];
//     var count = rows.length;
//     rows.forEach(function(_row) {
//         app.models.IndicadorTipo.create(_row, function(err, model) {
//             if (err) throw err;

//             console.log('Created:', model);

//             count--;
//             if (count === 0)
//                 ds.disconnect();
//         });
//     });
// });

// ds.automigrate('Usuario', function(err) {
//     if (err) throw err;

//     var users = [{
//         email: 'teste@teste.com',
//         password: '123@teste',
//         nome: 'Teste',
//         situacao: true,
//         pusu_id: 1,
//         admin: true,
//         emp_id: 1
//             //   createdAt: new Date(),
//             //   lastModifiedAt: new Date()
//     }];
//     var count = users.length;
//     users.forEach(function(user) {
//         app.models.Usuario.create(user, function(err, model) {
//             if (err) throw err;

//             console.log('Created:', model);

//             count--;
//             if (count === 0)
//                 ds.disconnect();
//         });
//     });
// });